package mx.unitec.practica4

import android.content.Context
import androidx.preference.PreferenceManager

private const val PREF_RFC = "dataRFC"

object DataPreferences {

    fun getStoreRFC(context: Context) : String {

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)

        return prefs.getString(PREF_RFC,"")!!

    }

    fun setStoredRFC(context: Context, query: String) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(PREF_RFC, query)
            .apply()
    }
}